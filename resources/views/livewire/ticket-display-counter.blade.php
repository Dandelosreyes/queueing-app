<div class="flex h-screen bg-gray-500 text-white">
    <div class="m-auto">
        <div class="grid grid-cols-2">
            <div class="border-red-500 border-4 px-4 py-4">
                <h1 class="text-5xl">
                    TICKET #
                </h1>
                <h3 class="text-5xl font-bold text-red">
                    {{ $ticketNumber }}
                </h3>
            </div>
            <div class="border-red-500 border-4 px-4 py-4">
                <h1 class="text-5xl">
                    Please Proceed to Counter #
                </h1>
                <h3 class="text-5xl font-bold text-red">
                    {{ $windowNumber }}
                </h3>
            </div>
        </div>
        <button class="flex-1 text-5xl py-4 px-4 border-green-500 border-4 w-full" wire:click="nextTicket">Next</button>
    </div>
</div>