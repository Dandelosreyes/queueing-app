<x-app-layout><div class="w-full sm:px-6">
        <div class="px-4 md:px-10 py-4 md:py-7 bg-gray-100 rounded-tl-lg rounded-tr-lg">
            <div class="sm:flex items-center justify-between">
                <p tabindex="0" class="focus:outline-none text-base sm:text-lg md:text-xl lg:text-2xl font-bold leading-normal text-gray-800">Projects</p>
                <div>
                    <button class="focus:ring-2 focus:ring-offset-2 focus:ring-indigo-600 inline-flex sm:ml-3 mt-4 sm:mt-0 items-start justify-start px-6 py-3 bg-indigo-700 hover:bg-indigo-600 focus:outline-none rounded">
                        <p class="text-sm font-medium leading-none text-white">New Project</p>
                    </button>
                </div>
            </div>
        </div>
        <div class="bg-white shadow px-4 md:px-10 pt-4 md:pt-7 pb-5 overflow-y-auto">
            <table class="w-full whitespace-nowrap">
                <thead>
                    <tr tabindex="0" class="focus:outline-none h-16 w-full text-sm leading-none text-gray-800">
                        <th class="font-normal text-center pl-4">Name</th>
                        <th class="font-normal text-center pl-12">Date Created</th>
                        <th class="font-normal text-center pl-12">Action</th>
                    </tr>
                </thead>
                <tbody class="w-full">
                @foreach($services as $service)
                    <tr tabindex="0" class="focus:outline-none h-20 text-sm leading-none text-gray-800 bg-white hover:bg-gray-100 border-b border-t border-gray-100">
                        <td class="pl-4 cursor-pointer">
                            <p class="font-medium">
                                {{ $service->name }}
                            </p>
                        </td>
                        <td class="pl-12">
                            <p class="text-sm font-medium leading-none text-gray-800">
                                {{ $service->created_at->diffForHumans() }}
                            </p>
                        </td>
                        <td class="">
                            <div class="flex items-center">
                                <a href="#" class="px-4 py-4 border-red-500 bg-red-400">Edit</a>
                                <a href="#" class="px-4 py-4 border-red-500 bg-red-400">Delete</a>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</x-app-layout>