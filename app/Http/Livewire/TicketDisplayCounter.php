<?php

namespace App\Http\Livewire;

use Livewire\Component;

class TicketDisplayCounter extends Component
{

	public $ticketNumber;
	public $windowNumber;

	public function mount()
	{
		$this->ticketNumber = 1;
		$this->windowNumber = rand(1,100);
	}

	public function nextTicket()
	{
		$this->ticketNumber++;
		$this->windowNumber = rand(1,100);
	}

	public function render()
	{
		return view('livewire.ticket-display-counter');
	}
}