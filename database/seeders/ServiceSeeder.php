<?php

namespace Database\Seeders;

use Domain\Services\Models\Service;
use Illuminate\Database\Seeder;

class ServiceSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		collect(['assessments', 'payments'])
			->each(function ($service, $key) {
				$row = Service::create([
					'name' => $service
				]);

				$row->windows()->create([
					'window' => $key + 1
				]);
			});
	}
}
