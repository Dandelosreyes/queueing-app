<?php

namespace Database\Seeders;

use Domain\User\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$administrator = new User;
		$administrator->name = 'Administrator One';
		$administrator->email = 'admin@example.com';
		$administrator->password = bcrypt('123');
		$administrator->save();
	}
}
