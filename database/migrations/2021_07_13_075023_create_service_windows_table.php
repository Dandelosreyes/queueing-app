<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServiceWindowsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service_windows', function (Blueprint $table) {
            $table->id();
            $table->foreignIdFor(\Domain\Services\Models\Service::class)
	            ->constrained()
	            ->cascadeOnUpdate()
	            ->cascadeOnDelete();
            $table->string('window')
                ->unique();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('service_windows');
    }
}
