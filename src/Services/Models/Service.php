<?php

namespace Domain\Services\Models;

use Domain\ServiceWindows\Models\ServiceWindow;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    use HasFactory;

    protected $fillable = [
    	'name'
    ];

    public function windows()
    {
    	return $this->hasMany(ServiceWindow::class);
    }
}
