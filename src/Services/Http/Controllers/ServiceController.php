<?php

namespace Domain\Services\Http\Controllers;

use Domain\Services\Repositories\ServiceRepository;

class ServiceController extends \App\Http\Controllers\Controller
{

	public function __construct(
		public ServiceRepository $serviceRepository
	){}

	public function index()
	{
		return view('services.list-services')
			->with([
				'services' => $this->serviceRepository->getAllRecords()
			]);
	}
}