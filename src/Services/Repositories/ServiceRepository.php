<?php

namespace Domain\Services\Repositories;

use Domain\Services\Models\Service;

class ServiceRepository
{
	public function __construct(
		public Service $service
	){}

	public function getAllRecords()
	{
		return $this->service->all();
	}
}