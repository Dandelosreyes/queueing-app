<?php

namespace Domain\ServiceWindows\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ServiceWindow extends Model
{
    use HasFactory;

    protected $fillable = [
    	'window'
    ];
}
